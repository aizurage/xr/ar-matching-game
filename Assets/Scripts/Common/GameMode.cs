using System;
using System.Collections;
using System.Collections.Generic;

namespace MatchingGame
{
    namespace Common
    {
        /// <summary>
        /// ゲームモード
        /// ToDO モード種別再考
        /// </summary>
        public enum GameMode
        {
            MODE_2x2,
            MODE_4x4,
            MODE_4x8,
            MODE_4x13,
        };

        static public class CommonFunction
        {
            static public int GetCardNum(GameMode mode)
            {
                switch (mode)
                {
                    case GameMode.MODE_2x2:
                        return 2 * 2;
                    case GameMode.MODE_4x4:
                        return 4 * 4;
                    case GameMode.MODE_4x8:
                        return 4 * 8;
                    case GameMode.MODE_4x13:
                        return 4 * 13;
                    default:
                        throw new InvalidProgramException("場合が網羅されていません");

                }
            }

            /// <summary>
            /// (vertical, horizontal)
            /// </summary>
            /// <param name="mode"></param>
            /// <returns></returns>
            static public (int, int) GetCardDimension(GameMode mode)
            {
                switch (mode)
                {
                    case GameMode.MODE_2x2:
                        return (2, 2);
                    case GameMode.MODE_4x4:
                        return (4, 4);
                    case GameMode.MODE_4x8:
                        return (4, 8);
                    case GameMode.MODE_4x13:
                        return (4, 13);
                    default:
                        throw new InvalidProgramException("場合が網羅されていません");

                }
            }
        }
    }
}
