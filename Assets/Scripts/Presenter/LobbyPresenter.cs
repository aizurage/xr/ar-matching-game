using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VContainer;
using VContainer.Unity;
using MatchingGame.View;
using MatchingGame.Common;
using UniRx;
using Photon.Pun;
using Photon.Realtime;

namespace MatchingGame
{
    namespace Presenter
    {
        public class LobbyPresenter : IStartable, IDisposable, IConnectionCallbacks, IInRoomCallbacks, IMatchmakingCallbacks
        {
            private const string PLAYER_NAME_PREF_KEY = "PlayerName";
            private const byte MAX_PLAYER_PER_ROOM = 4;

            [Inject] JoinMenberPanelView _JoinMenberPanelView;
            [Inject] ConnectLabelView _ConnectLabelView;
            [Inject] JoinButtonView _JoinButtonView;
            [Inject] StartButtonView _StartButtonView;
            [Inject] ExitButtonView _ExitButtonView;
            [Inject] NameInputFieldView _NameInputFieldView;

            private IDisposable _NameInputFieldOnValueChangedObservable;
            private IDisposable _JoinButtonButtonClickObservable;
            private IDisposable _StartButtonButtonClickObservable;
            private IDisposable _ExitButtonButtonClickObservable;

            string _PlayerName;
            string _GameVersion = "1";


            void IStartable.Start()
            {
                _JoinMenberPanelView.RemoveAllMember();
                _ConnectLabelView.Hide();
                _StartButtonView.Hide();
                _ExitButtonView.Hide();

                // 前回入力した名前があれば表示する
                string defaultName = string.Empty;
                if (PlayerPrefs.HasKey(PLAYER_NAME_PREF_KEY))
                {
                    _PlayerName = PlayerPrefs.GetString(PLAYER_NAME_PREF_KEY);
                    _NameInputFieldView.SetText(_PlayerName);
                }

                _NameInputFieldOnValueChangedObservable = _NameInputFieldView.OnValueChangedObservable.Subscribe(text => SetPlayerName(text));
                _JoinButtonButtonClickObservable = _JoinButtonView.ButtonClickObservable.Subscribe(_ => JoinRoom());
                _StartButtonButtonClickObservable = _StartButtonView.ButtonClickObservable.Subscribe(gameMode => NextScene(gameMode));
                _ExitButtonButtonClickObservable = _ExitButtonView.ButtonClickObservable.Subscribe(_ => ExitRoom());

                PhotonNetwork.AddCallbackTarget(this);
                PhotonNetwork.AutomaticallySyncScene = true;
                if (!PhotonNetwork.IsConnected)
                {
                    PhotonNetwork.GameVersion = _GameVersion;
                    PhotonNetwork.ConnectUsingSettings();
                    _JoinButtonView.Hide(); // JOINボタンは接続が完了してから表示する
                } 
            }


            private void SetPlayerName(string text)
            {
                if (string.IsNullOrEmpty(text))
                {
                    Debug.LogError("Player Name is null or empty");
                    return;
                }
                _PlayerName = text;
                PlayerPrefs.SetString(PLAYER_NAME_PREF_KEY, text);
            }

            private void JoinRoom()
            {
                _JoinButtonView.Hide();
                _ConnectLabelView.Show();

                PhotonNetwork.NickName = _PlayerName;
                PhotonNetwork.JoinRandomRoom();
            }

            private void ExitRoom()
            {
                _StartButtonView.Hide();
                _ExitButtonView.Hide();
                PhotonNetwork.LeaveRoom();
            }

            private void NextScene(int gameMode)
            {
                VenuePresenter.GameMode = GetGameMode(gameMode); // VenuePresenterをUnityのスコープから外したので、GetComponentできなくなった。
                if( Application.platform == RuntimePlatform.WindowsPlayer
                 || Application.platform == RuntimePlatform.OSXPlayer
                 || Application.platform == RuntimePlatform.OSXEditor 
                 || Application.platform == RuntimePlatform.WindowsEditor
                 || Application.platform == RuntimePlatform.LinuxEditor)
                 {
                     PhotonNetwork.LoadLevel("Venue3DScene");
                 } else {
                     PhotonNetwork.LoadLevel("VenueScene");
                 }
            }

            void IDisposable.Dispose()
            {
                PhotonNetwork.RemoveCallbackTarget(this);
                _NameInputFieldOnValueChangedObservable?.Dispose();
                _JoinButtonButtonClickObservable?.Dispose();
                _StartButtonButtonClickObservable?.Dispose();
                _ExitButtonButtonClickObservable?.Dispose();
            }

            /**
             * Dropdownで選択された値からゲームのモードを取得する。
             * @params dropdownselectedValue : 選択されているドロップダウンの値
             * @return : ゲームモード
             */
            private GameMode GetGameMode(int dropdownselectedValue)
            {
                switch (dropdownselectedValue)
                {
                    case 0:
                        return GameMode.MODE_2x2;
                    case 1:
                        return GameMode.MODE_4x4;
                    case 2:
                        return GameMode.MODE_4x8;
                    case 3:
                        return GameMode.MODE_4x13;
                }
                throw new ArgumentException("使用できない値が選択された");
            }


            #region IConnectionCallbacks

            public void OnConnected()
            {
                return;
            }

            public void OnConnectedToMaster()
            {
                _JoinButtonView.Show();
            }

            public void OnDisconnected(DisconnectCause cause)
            {
                _JoinButtonView.Hide();
                return;
            }

            public void OnRegionListReceived(RegionHandler regionHandler)
            {
                return;
            }

            public void OnCustomAuthenticationResponse(Dictionary<string, object> data)
            {
                return;
            }

            public void OnCustomAuthenticationFailed(string debugMessage)
            {
                return;
            }

            #endregion

            #region IInRoomCallbacks
            public void OnPlayerEnteredRoom(Player newPlayer)
            {
                _JoinMenberPanelView.AddMember(newPlayer.ActorNumber, newPlayer.NickName);
                return;
            }
            public void OnPlayerLeftRoom(Player otherPlayer)
            {
                _JoinMenberPanelView.RemoveMember(otherPlayer.ActorNumber);
                return;
            }
            public void OnRoomPropertiesUpdate(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
            {
                return;
            }
            public void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
            {
                return;
            }
            public void OnMasterClientSwitched(Player newMasterClient)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                    _StartButtonView.Show();
                }
            }

            #endregion

            #region IMatchmakingCallbacks
            public void OnFriendListUpdate(List<FriendInfo> friendList)
            {
                return;
            }
            public void OnCreatedRoom()
            {
                return;
            }
            public void OnCreateRoomFailed(short returnCode, string message)
            {
                Debug.Log("OnCreateRoomFailed");
                return;
            }
            public void OnJoinedRoom()
            {
                _NameInputFieldView.Hide();
                _ConnectLabelView.Hide();
                _ExitButtonView.Show();

                if (PhotonNetwork.IsMasterClient)
                {
                    _StartButtonView.Show();
                }

                foreach (var player in PhotonNetwork.PlayerList)
                {
                    _JoinMenberPanelView.AddMember(player.ActorNumber, player.NickName);
                }

                return;
            }
            public void OnJoinRoomFailed(short returnCode, string message)
            {
                Debug.Log("OnJoinRoomFailed");
                return;
            }
            public void OnJoinRandomFailed(short returnCode, string message)
            {
                Debug.Log("OnJoinRandomFailed : create room.");
                PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = MAX_PLAYER_PER_ROOM });
            }
            public void OnLeftRoom()
            {
                _JoinMenberPanelView.RemoveAllMember();
                _ConnectLabelView.Hide();
                _JoinButtonView.Show();
                _StartButtonView.Hide();
                _ExitButtonView.Hide();
                _NameInputFieldView.Show();
            }

            #endregion
        }
    }
}
