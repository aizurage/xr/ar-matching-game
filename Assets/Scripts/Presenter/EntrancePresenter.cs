using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UniRx;
using MatchingGame.View;
using MatchingGame.Common;

namespace MatchingGame
{
    namespace Presenter
    {
        public class EntrancePresenter : MonoBehaviour
        {
            [SerializeField] EntranceView _EntranceView;

            IDisposable _NewButtonDisposable;

            // Start is called before the first frame update
            void Start()
            => _NewButtonDisposable = _EntranceView.NewButtonObservable.Subscribe(_ => ToNextScene());
            
            /**
                * Veneuシーンをロードする
                */
            private void ToNextScene()
            {
                SceneManager.LoadScene("LobbyScene");
            }

            private void OnDestroy()
            => _NewButtonDisposable?.Dispose();
            
        }

    }
}