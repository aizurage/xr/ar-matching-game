using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class OKView : MonoBehaviour
        {
            private Subject<object> _Subject;

            public IObservable<object> OKButtonClickObservable => _Subject;        
            
            public void Show()
            => this.gameObject.SetActive(true);

            public void Push() 
            => _Subject.OnNext(this);

            public void Hide()
            => this.gameObject.SetActive(false);
            
            private void Awake() {
                _Subject = new Subject<object>();
                this.gameObject.SetActive(false);
            }

            private void OnDestroy()
            => _Subject.Dispose();
        }

    }
}