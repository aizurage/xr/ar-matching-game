using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        /// <summary>
        /// ARを使用しない、3D空間用のARView
        /// 平面タッチのスタブ
        /// </summary>
        public class DummyARView : MonoBehaviour, IARView
        {
            private Subject<Vector3> _Subject;
            // 平面がタッチされた時のオブザーバ
            // Vector3 => タッチされた場所
            public IObservable<Vector3> TouchPlaneObservable => _Subject;

            private bool _IsFirst = true;

            public Vector3 GetPlayerPosition()
            {
                return GameObject.Find("PlayerFor3D").transform.position;
            }

            private void Awake()
            {
                _Subject = new Subject<Vector3>();
            }

            // Update is called once per frame
            void Update()
            {
                if (_IsFirst)
                {
                    _IsFirst = false;
                    _Subject.OnNext(Vector3.zero);
                }
            }
        }
    }
}
