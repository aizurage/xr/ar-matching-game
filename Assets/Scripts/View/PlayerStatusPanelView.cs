using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace MatchingGame
{
    namespace View
    {
        public class PlayerStatusPanelView : MonoBehaviour
        {
            [SerializeField] List<GameObject> _MemberPanels;

            private Dictionary<int, GameObject> _PlayerIdPanelPairs = new Dictionary<int, GameObject>();
            private int _MemberCount = 0;

            public void AddPlayer(string name, int id)
            {
                _MemberPanels[_MemberCount].transform.Find("NameText").GetComponent<Text>().text = name;
                _MemberPanels[_MemberCount].SetActive(true);
                _PlayerIdPanelPairs[id] = _MemberPanels[_MemberCount];
                _MemberCount++;
            }

            public void ClearAllStatus()
            {
                foreach (GameObject gameObject in _MemberPanels)
                {
                    gameObject.transform.Find("StatusText").GetComponent<Text>().text = "";
                }
            }

            public void UpdateStatus(int id, string status)
            {
                _PlayerIdPanelPairs[id].transform.Find("StatusText").GetComponent<Text>().text = status;
            }

            public void Show()
            {
                gameObject.SetActive(true);
            }

            public void Hide()
            {
                gameObject.SetActive(false);
            }
        }
    }
}
