using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

namespace MatchingGame
{
    namespace View
    {
        public class NewGameButtonView : MonoBehaviour
        {
            private Subject<Unit> _Subject;

            public IObservable<Unit> ButtonClickObservable => _Subject;

            // めくる対象のカード
            private CardView _CardView;

            public void Show()
            => gameObject.SetActive(true);

            public void Hide()
            => gameObject.SetActive(false);

            public void PushNewGameButton()
            => _Subject.OnNext(Unit.Default);

            private void Awake()
            => _Subject = new Subject<Unit>();


            private void OnDestroy()
            => _Subject.Dispose();
        }
    }
}