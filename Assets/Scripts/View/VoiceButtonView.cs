using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;


namespace MatchingGame
{
    namespace View
    {
        public class VoiceButtonView : MonoBehaviour
        {
            [SerializeField] GameObject _DisableView;
            private Subject<bool> _Subject;

            private bool _MicrophoneEnable = false;

            public IObservable<bool> VoiceButtonClickObservable => _Subject;

            public void Show()
            => this.gameObject.SetActive(true);

            public void Push()
            => _Subject.OnNext(!_MicrophoneEnable);

            public void Hide()
            => this.gameObject.SetActive(false);

            public void SwitchMicrophone()
            {
                _MicrophoneEnable = !_MicrophoneEnable;
                _DisableView.SetActive(_MicrophoneEnable);
            }

            private void Awake()
            {
                _Subject = new Subject<bool>();
                _DisableView.SetActive(_MicrophoneEnable);
            }

            private void OnDestroy()
            => _Subject.Dispose();
        }
    }
}
